# Drone Delivery Android Client

The project Aid Chain is conceived and developed in response to IBM Natural Disaster Call for Code Challenge 2018 by the Ukrainian team 

  <img width="400" height="400" src="https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-composer-model/uploads/9c1a84bbc460479d06f1f9496e54ed75/ItunesArtwork_2x.png">


# High-level architecture

Drone delivery field operation client used to start, navigate, track, and finish deliveries to requests recorded on the blockchain in the real-time mode.

Drones equipped with GPS module and wi-fi/4G connectivity are used to deliver aid buckets;

Near real time reports and in-depth/ad hoc analytics are based on Watson IoT connected to drones, Watson Studio, and Cloudant;

![alt tag](https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-bebop2-client/uploads/eedf54cdc900ef3487f8f3f7556b79a3/Screen_Shot_2018-09-18_at_5.44.57_PM.png)

# Delivery flow

Buckets created by inventory transfered to target location by the drone. 
Drone starts delivery by submitting a transaction into the blockchain.

 <img width="1000" height="500" src="https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-bebop2-client/uploads/a3ac978185b742cc85bedfe18092a533/3-start-delivery-android.png">

When drone is close enough to target place it could deliver aid and finish request by sending corresponging transaction into the blockchain:

<img width="1000" height="500" src="https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-bebop2-client/uploads/da826ccaded35bf6833e02e0d1f76743/4-finish-delivery-android.png">

All information about flight is transmitted and stored:

<img width="1000" height="500" src="https://gitlab.com/softserve-blockchain/callforcode-2018/aid-chain-bebop2-client/uploads/2f4aa1f718d076f62df11a1f73ca9122/3-drone-telemetry-mqtt-cloudant.png">

# Aidchain team:

Andriy Shapochka ashapoch@softserveinc.com

Dmytro Ovcharenko dovchar@softserveinc.com

Denys Doronin ddor@softserveinc.com