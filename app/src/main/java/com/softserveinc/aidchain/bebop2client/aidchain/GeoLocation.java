package com.softserveinc.aidchain.bebop2client.aidchain;

/**
 * @author ashapoch
 * @since 9/2/18.
 */
public class GeoLocation {
    private final double latitude;
    private final double longitude;

    public GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
