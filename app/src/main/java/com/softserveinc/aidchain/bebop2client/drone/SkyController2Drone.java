package com.softserveinc.aidchain.bebop2client.drone;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import com.parrot.arsdk.arcommands.*;
import com.parrot.arsdk.arcontroller.*;
import com.parrot.arsdk.ardiscovery.*;
import com.parrot.arsdk.arutils.ARUtilsException;
import com.parrot.arsdk.arutils.ARUtilsManager;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import static com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING;
import static com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_STOPPED;
import static com.parrot.arsdk.arcontroller.ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK;
import static com.parrot.arsdk.arcontroller.ARFeatureARDrone3.*;
import static com.parrot.arsdk.arcontroller.ARFeatureCommon.*;
import static com.parrot.arsdk.arcontroller.ARFeatureSkyController.*;
import static com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM.*;
import static com.parrot.arsdk.arutils.ARUTILS_DESTINATION_ENUM.ARUTILS_DESTINATION_DRONE;
import static com.parrot.arsdk.arutils.ARUTILS_FTP_TYPE_ENUM.ARUTILS_FTP_TYPE_GENERIC;

public class SkyController2Drone {
    private static final String TAG = "SkyController2Drone";

    public interface Listener {
        /**
         * Called when the connection to the SkyController2 changes
         * Called in the main thread
         *
         * @param state the droneState of the SkyController2
         */
        void onSkyController2ConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state);

        /**
         * Called when the connection to the drone changes
         * Called in the main thread
         *
         * @param state the droneState of the drone
         */
        void onDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state);

        /**
         * Called when the SkyController2 battery charge changes
         * Called in the main thread
         *
         * @param batteryPercentage the battery remaining (in percent)
         */
        void onSkyController2BatteryChargeChanged(int batteryPercentage);

        /**
         * Called when the battery charge changes
         * Called in the main thread
         *
         * @param batteryPercentage the battery remaining (in percent)
         */
        void onDroneBatteryChargeChanged(int batteryPercentage);

        /**
         * Called when the piloting droneState changes
         * Called in the main thread
         *
         * @param state the piloting droneState of the drone
         */
        void onPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state);

        /**
         * Called when a picture is taken
         * Called on a separate thread
         *
         * @param error ERROR_OK if picture has been taken, otherwise describe the error
         */
        void onPictureTaken(ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error);

        /**
         * Called when the video decoder should be configured
         * Called on a separate thread
         *
         * @param codec the codec to configure the decoder with
         */
        void configureDecoder(ARControllerCodec codec);

        /**
         * Called when a video frame has been received
         * Called on a separate thread
         *
         * @param frame the video frame
         */
        void onFrameReceived(ARFrame frame);

        /**
         * Called before medias will be downloaded
         * Called in the main thread
         *
         * @param nbMedias the number of medias that will be downloaded
         */
        void onMatchingMediasFound(int nbMedias);

        /**
         * Called each time the progress of a download changes
         * Called in the main thread
         *
         * @param mediaName the name of the media
         * @param progress  the progress of its download (from 0 to 100)
         */
        void onDownloadProgressed(String mediaName, int progress);

        /**
         * Called when a media download has ended
         * Called in the main thread
         *
         * @param mediaName the name of the media
         */
        void onDownloadComplete(String mediaName);

        void onSpeedChanged(double speedX, double speedY, double speedZ);

        void onEvent(Map<String, Object> eventMap);
    }

    private final List<Listener> listeners;

    private final Handler handler;
    private final Context context;

    private ARDeviceController deviceController;
    private SDCardModule sdCardModule;
    private ARCONTROLLER_DEVICE_STATE_ENUM skyController2State;
    private ARCONTROLLER_DEVICE_STATE_ENUM droneState;
    private ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM flyingState;
    private String currentRunId;
    private ARDiscoveryDeviceService discoveryDeviceService;
    private ARUtilsManager ftpListManager;
    private ARUtilsManager ftpQueueManager;

    private DroneState state;

    public SkyController2Drone(Context context, @NonNull ARDiscoveryDeviceService deviceService) {

        this.context = context;
        listeners = new ArrayList<>();
        state = new DroneState();
        discoveryDeviceService = deviceService;

        // needed because some callbacks will be called on the main thread
        handler = new Handler(context.getMainLooper());

        skyController2State = ARCONTROLLER_DEVICE_STATE_STOPPED;
        droneState = ARCONTROLLER_DEVICE_STATE_STOPPED;

        // if the product type of the deviceService match with the types supported
        ARDISCOVERY_PRODUCT_ENUM productType = ARDiscoveryService.getProductFromProductID(deviceService.getProductID());
        if (ARDISCOVERY_PRODUCT_SKYCONTROLLER_2.equals(productType) ||
                ARDISCOVERY_PRODUCT_SKYCONTROLLER_NG.equals(productType) ||
                ARDISCOVERY_PRODUCT_SKYCONTROLLER_2P.equals(productType)) {

            ARDiscoveryDevice discoveryDevice = createDiscoveryDevice(deviceService);
            if (discoveryDevice != null) {
                deviceController = createDeviceController(discoveryDevice);
                discoveryDevice.dispose();
            }

            try {
                ftpListManager = new ARUtilsManager();
                ftpQueueManager = new ARUtilsManager();

                ftpListManager.initFtp(this.context, deviceService, ARUTILS_DESTINATION_DRONE,
                        ARUTILS_FTP_TYPE_GENERIC);
                ftpQueueManager.initFtp(this.context, deviceService, ARUTILS_DESTINATION_DRONE,
                        ARUTILS_FTP_TYPE_GENERIC);

                sdCardModule = new SDCardModule(ftpListManager, ftpQueueManager);
                sdCardModule.addListener(sdCardModuleListener);
            } catch (ARUtilsException e) {
                Log.e(TAG, "Exception", e);
            }

        } else {
            Log.e(TAG, "DeviceService type is not supported by SkyController2Drone");
        }
    }

    public void dispose() {
        if (deviceController != null)
            deviceController.dispose();
        if (ftpListManager != null)
            ftpListManager.closeFtp(context, discoveryDeviceService);
        if (ftpQueueManager != null)
            ftpQueueManager.closeFtp(context, discoveryDeviceService);
    }

    //region Listener functions
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }
    //endregion Listener

    /**
     * Connect to the drone
     *
     * @return true if operation was successful.
     * Returning true doesn't mean that device is connected.
     * You can be informed of the actual connection through {@link Listener#onSkyController2ConnectionChanged}
     */
    public boolean connect() {
        boolean success = false;
        if ((deviceController != null) && (ARCONTROLLER_DEVICE_STATE_STOPPED.equals(skyController2State))) {
            ARCONTROLLER_ERROR_ENUM error = deviceController.start();
            if (error == ARCONTROLLER_OK) {
                success = true;
            }
        }
        return success;
    }

    /**
     * Disconnect from the drone
     *
     * @return true if operation was successful.
     * Returning true doesn't mean that device is disconnected.
     * You can be informed of the actual disconnection through {@link Listener#onSkyController2ConnectionChanged}
     */
    public boolean disconnect() {
        boolean success = false;
        if ((deviceController != null) && (ARCONTROLLER_DEVICE_STATE_RUNNING.equals(skyController2State))) {
            ARCONTROLLER_ERROR_ENUM error = deviceController.stop();
            if (error == ARCONTROLLER_OK) {
                success = true;
            }
        }
        return success;
    }

    /**
     * Get the current connection droneState
     *
     * @return the connection droneState of the drone
     */
    public ARCONTROLLER_DEVICE_STATE_ENUM getSkyController2ConnectionState() {
        return skyController2State;
    }

    /**
     * Get the current connection droneState
     *
     * @return the connection droneState of the drone
     */
    public ARCONTROLLER_DEVICE_STATE_ENUM getDroneConnectionState() {
        return droneState;
    }

    /**
     * Get the current flying droneState
     *
     * @return the flying droneState
     */
    public ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM getFlyingState() {
        return flyingState;
    }

    public void takeOff() {
        if ((deviceController != null) &&
                (skyController2State.equals(ARCONTROLLER_DEVICE_STATE_RUNNING)) &&
                (deviceController.getExtensionState().equals(ARCONTROLLER_DEVICE_STATE_RUNNING))) {
            deviceController.getFeatureARDrone3().sendPilotingTakeOff();
        }
    }

    public void land() {
        if ((deviceController != null) &&
                (skyController2State.equals(ARCONTROLLER_DEVICE_STATE_RUNNING)) &&
                (deviceController.getExtensionState().equals(ARCONTROLLER_DEVICE_STATE_RUNNING))) {
            deviceController.getFeatureARDrone3().sendPilotingLanding();
        }
    }

    public void emergency() {
        if ((deviceController != null) &&
                (skyController2State.equals(ARCONTROLLER_DEVICE_STATE_RUNNING)) &&
                (deviceController.getExtensionState().equals(ARCONTROLLER_DEVICE_STATE_RUNNING))) {
            deviceController.getFeatureARDrone3().sendPilotingEmergency();
        }
    }

    public void takePicture() {
        if ((deviceController != null) &&
                (skyController2State.equals(ARCONTROLLER_DEVICE_STATE_RUNNING)) &&
                (deviceController.getExtensionState().equals(ARCONTROLLER_DEVICE_STATE_RUNNING))) {
            deviceController.getFeatureARDrone3().sendMediaRecordPictureV2();
        }
    }

    /**
     * Download the last flight medias
     * Uses the run id to download all medias related to the last flight
     * If no run id is available, download all medias of the day
     */
    public void getLastFlightMedias() {
        String runId = currentRunId;
        if ((runId != null) && !runId.isEmpty()) {
            sdCardModule.getFlightMedias(runId);
        } else {
            Log.e(TAG, "RunID not available, fallback to the day's medias");
            sdCardModule.getTodaysFlightMedias();
        }
    }

    public void cancelGetLastFlightMedias() {
        sdCardModule.cancelGetFlightMedias();
    }

    private ARDiscoveryDevice createDiscoveryDevice(@NonNull ARDiscoveryDeviceService service) {
        ARDiscoveryDevice device = null;
        try {
            device = new ARDiscoveryDevice(context, service);
        } catch (ARDiscoveryException e) {
            Log.e(TAG, "Exception", e);
            Log.e(TAG, "Error: " + e.getError());
        }

        return device;
    }

    private ARDeviceController createDeviceController(@NonNull ARDiscoveryDevice discoveryDevice) {
        ARDeviceController deviceController = null;
        try {
            deviceController = new ARDeviceController(discoveryDevice);

            deviceController.addListener(deviceControllerListener);
            deviceController.addStreamListener(mStreamListener);
        } catch (ARControllerException e) {
            Log.e(TAG, "Exception", e);
        }

        return deviceController;
    }

    //region notify listener block
    private void notifySkyController2ConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onSkyController2ConnectionChanged(state);
        }
    }

    private void notifyDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onDroneConnectionChanged(state);
        }
    }

    private void notifySkyController2BatteryChanged(int battery) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onSkyController2BatteryChargeChanged(battery);
        }
    }

    private void notifyDroneBatteryChanged1(int battery) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        listenersCpy.forEach((listener) -> listener.onDroneBatteryChargeChanged(battery));
        for (Listener listener : listenersCpy) {
            listener.onDroneBatteryChargeChanged(battery);
        }
    }

    private void notifyDroneBatteryChanged(int battery) {
        notifyListeners((listener) -> listener.onDroneBatteryChargeChanged(battery));
    }

    private void notifySpeedChanged(double speedX, double speedY, double speedZ) {
        notifyListeners((listener) -> listener.onSpeedChanged(speedX, speedY, speedZ));
    }

    private void notifyEvent(Map<String, Object> eventMap) {
        notifyListeners((listener) -> listener.onEvent(eventMap));
    }

    private void notifyListeners(Consumer<? super SkyController2Drone.Listener> action) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        listenersCpy.forEach(action);
    }

    private void notifyPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onPilotingStateChanged(state);
        }
    }

    private void notifyPictureTaken(ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onPictureTaken(error);
        }
    }

    private void notifyConfigureDecoder(ARControllerCodec codec) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.configureDecoder(codec);
        }
    }

    private void notifyFrameReceived(ARFrame frame) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onFrameReceived(frame);
        }
    }

    private void notifyMatchingMediasFound(int nbMedias) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onMatchingMediasFound(nbMedias);
        }
    }

    private void notifyDownloadProgressed(String mediaName, int progress) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onDownloadProgressed(mediaName, progress);
        }
    }

    private void notifyDownloadComplete(String mediaName) {
        List<Listener> listenersCpy = new ArrayList<>(listeners);
        for (Listener listener : listenersCpy) {
            listener.onDownloadComplete(mediaName);
        }
    }
    //endregion notify listener block

    private final SDCardModule.Listener sdCardModuleListener = new SDCardModule.Listener() {
        @Override
        public void onMatchingMediasFound(final int nbMedias) {
            handler.post(() -> notifyMatchingMediasFound(nbMedias));
        }

        @Override
        public void onDownloadProgressed(final String mediaName, final int progress) {
            handler.post(() -> notifyDownloadProgressed(mediaName, progress));
        }

        @Override
        public void onDownloadComplete(final String mediaName) {
            handler.post(() -> notifyDownloadComplete(mediaName));
        }
    };

    public static class DroneState {
        AtomicInteger droneBattery = new AtomicInteger(-1);
        AtomicInteger skyControllerBattery = new AtomicInteger(-1);
        AtomicReference<ARCONTROLLER_DEVICE_STATE_ENUM> skyController2State = new AtomicReference<>();
        AtomicReference<ARCONTROLLER_DEVICE_STATE_ENUM> droneState = new AtomicReference<>();
        AtomicReference<ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM> flyingState = new AtomicReference<>();
        AtomicReference<ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM> pictureTakenError = new AtomicReference<>();
        AtomicReference<String> currentRunId = new AtomicReference<>();
        AtomicReference<Double> dX = new AtomicReference<>();
        AtomicReference<Double> dY = new AtomicReference<>();
        AtomicReference<Double> dZ = new AtomicReference<>();
        AtomicReference<Double> dPsi = new AtomicReference<>();
        AtomicReference<ARCOMMANDS_ARDRONE3_PILOTINGEVENT_MOVEBYEND_ERROR_ENUM> moveError = new AtomicReference<>();
        AtomicReference<Double> speedX = new AtomicReference<>();
        AtomicReference<Double> speedY = new AtomicReference<>();
        AtomicReference<Double> speedZ = new AtomicReference<>();
        AtomicReference<Double> takeOffAltitude = new AtomicReference<>();
        AtomicReference<Double> gpsLatitude = new AtomicReference<>();
        AtomicReference<Double> gpsLongitude = new AtomicReference<>();
        AtomicReference<Double> seaLevelAltitude = new AtomicReference<>();
        AtomicInteger gpsLatitudeAccuracy = new AtomicInteger();
        AtomicInteger gpsLongitudeAccuracy = new AtomicInteger();
        AtomicInteger seaLevelAltitudeAccuracy = new AtomicInteger();
        AtomicInteger gpsSatelliteNumber = new AtomicInteger();

        @Override
        public String toString() {
            return new StringJoiner(", ", DroneState.class.getSimpleName() + "[", "]")
                    .add("droneBattery=" + droneBattery)
                    .add("skyControllerBattery=" + skyControllerBattery)
                    .add("skyController2State=" + skyController2State)
                    .add("droneState=" + droneState)
                    .add("flyingState=" + flyingState)
                    .add("pictureTakenError=" + pictureTakenError)
                    .add("currentRunId=" + currentRunId)
                    .add("dX=" + dX)
                    .add("dY=" + dY)
                    .add("dZ=" + dZ)
                    .add("dPsi=" + dPsi)
                    .add("moveError=" + moveError)
                    .add("speedX=" + speedX)
                    .add("speedY=" + speedY)
                    .add("speedZ=" + speedZ)
                    .add("takeOffAltitude=" + takeOffAltitude)
                    .add("gpsLatitude=" + gpsLatitude)
                    .add("gpsLongitude=" + gpsLongitude)
                    .add("seaLevelAltitude=" + seaLevelAltitude)
                    .add("gpsLatitudeAccuracy=" + gpsLatitudeAccuracy)
                    .add("gpsLongitudeAccuracy=" + gpsLongitudeAccuracy)
                    .add("seaLevelAltitudeAccuracy=" + seaLevelAltitudeAccuracy)
                    .add("gpsSatelliteNumber=" + gpsSatelliteNumber)
                    .toString();
        }

        public void log(String eventName) {
            Log.i(TAG, eventName + ": " + toString());
        }
    }

    private final ARDeviceControllerListener deviceControllerListener = new ARDeviceControllerListener() {
        @Override
        public void onStateChanged(ARDeviceController deviceController, ARCONTROLLER_DEVICE_STATE_ENUM newState,
                                   ARCONTROLLER_ERROR_ENUM error) {
            skyController2State = newState;
            state.skyController2State.set(newState);
            final Map<String, Object> eventMap = createEventMap("skyController2StateChanged");
            eventMap.put("skyController2State", safeName(state.skyController2State.get()));
            handler.post(() -> notifyEvent(eventMap));
            handler.post(() -> notifySkyController2ConnectionChanged(skyController2State));
            state.log("stateChanged");
        }

        @Override
        public void onExtensionStateChanged(ARDeviceController deviceController,
                                            ARCONTROLLER_DEVICE_STATE_ENUM newState, ARDISCOVERY_PRODUCT_ENUM product
                , String name, ARCONTROLLER_ERROR_ENUM error) {
            droneState = newState;
            state.droneState.set(newState);
            if (ARCONTROLLER_DEVICE_STATE_RUNNING.equals(droneState)) {
                SkyController2Drone.this.deviceController.startVideoStream();
            } else if (ARCONTROLLER_DEVICE_STATE_STOPPED.equals(droneState)) {
                sdCardModule.cancelGetFlightMedias();
            }
            final Map<String, Object> eventMap = createEventMap("droneStateChanged");
            eventMap.put("droneState", safeName(state.droneState.get()));
            handler.post(() -> notifyEvent(eventMap));
            handler.post(() -> notifyDroneConnectionChanged(droneState));
            state.log("extensionStateChanged");
        }

//        private void logData(ARControllerDictionary elementDictionary, String... keys) {
//            if (elementDictionary != null) {
//                ARControllerArgumentDictionary<Object> args =
//                        elementDictionary.get(ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY);
//                if (args != null) {
//                    for (String key : keys) {
//                        Object value = args.get(key);
//                        if (value != null) {
//                            Log.i(TAG, key + ": " + value);
//                        }
//                    }
//                }
//            }
//        }

        @Override
        public void onCommandReceived(ARDeviceController deviceController,
                                      ARCONTROLLER_DICTIONARY_KEY_ENUM commandKey,
                                      ARControllerDictionary elementDictionary) {
            if (elementDictionary == null) {
                return;
            }
            ARControllerArgumentDictionary<Object> args =
                    elementDictionary.get(ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY);
            if (args == null) {
                return;
            }
//            logData(elementDictionary,
//                    ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE,
//                    ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDX,
//                    ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDY,
//                    ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDZ,
//                    ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_ALTITUDECHANGED_ALTITUDE);
            final Map<String, Object> eventMap = createEventMap(safeName(commandKey));
            switch (commandKey) {
                case ARCONTROLLER_DICTIONARY_KEY_COMMON_COMMONSTATE_BATTERYSTATECHANGED:
                    state.droneBattery.set((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_COMMON_COMMONSTATE_BATTERYSTATECHANGED_PERCENT));
                    eventMap.put("droneBattery", state.droneBattery.get());
                    handler.post(() -> notifyEvent(eventMap));
                    handler.post(() -> notifyDroneBatteryChanged(state.droneBattery.get()));
                    state.log("commandReceived " + commandKey.name());
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_SKYCONTROLLER_SKYCONTROLLERSTATE_BATTERYCHANGED:
                    state.skyControllerBattery.set((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_SKYCONTROLLER_SKYCONTROLLERSTATE_BATTERYCHANGED_PERCENT));
                    eventMap.put("skyControllerBattery", state.skyControllerBattery.get());
                    handler.post(() -> notifyEvent(eventMap));
                    handler.post(() -> notifySkyController2BatteryChanged(state.skyControllerBattery.get()));
                    state.log("commandReceived " + commandKey.name());
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED:
                    final ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM fstate =
                            ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.getFromValue((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE));
                    flyingState = fstate;
                    state.flyingState.set(fstate);
                    eventMap.put("flyingState", safeName(state.flyingState.get()));
                    handler.post(() -> notifyEvent(eventMap));
                    handler.post(() -> notifyPilotingStateChanged(fstate));
                    state.log("commandReceived " + commandKey.name());
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED:
                    final ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error =
                            ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM.getFromValue((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR));
                    state.pictureTakenError.set(error);
                    handler.post(() -> notifyPictureTaken(error));
                    state.log("commandReceived " + commandKey.name());
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_COMMON_RUNSTATE_RUNIDCHANGED:
                    final String runID =
                            (String) args.get(ARCONTROLLER_DICTIONARY_KEY_COMMON_RUNSTATE_RUNIDCHANGED_RUNID);
                    state.currentRunId.set("".equals(runID) ? null : runID);
                    eventMap.put("runId", state.currentRunId.get());
                    handler.post(() -> notifyEvent(eventMap));
                    handler.post(() -> currentRunId = runID);
                    state.log("commandReceived " + commandKey.name());
                    break;
//                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED:
//                    double latitude =
//                            (double) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_LATITUDE);
//                    double longitude =
//                            (double) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_LONGITUDE);
//                    double seaLevelAltitude =
//                            (double) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_ALTITUDE);
//                    ARCOMMANDS_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_ORIENTATION_MODE_ENUM orientation_mode =
//                            ARCOMMANDS_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_ORIENTATION_MODE_ENUM.getFromValue((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_ORIENTATION_MODE));
//                    float heading =
//                            (float) ((Double) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_HEADING)).doubleValue();
//                    ARCOMMANDS_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_STATUS_ENUM status =
//                            ARCOMMANDS_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_STATUS_ENUM.getFromValue((Integer) args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_MOVETOCHANGED_STATUS));
//                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND:
                    state.dX.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND_DX));
                    state.dY.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND_DY));
                    state.dZ.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND_DZ));
                    state.dPsi.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND_DPSI));
                    ARCOMMANDS_ARDRONE3_PILOTINGEVENT_MOVEBYEND_ERROR_ENUM error1 = ARCOMMANDS_ARDRONE3_PILOTINGEVENT_MOVEBYEND_ERROR_ENUM.getFromValue((Integer)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGEVENT_MOVEBYEND_ERROR));
                    state.moveError.set(error1);
                    state.log("commandReceived " + commandKey.name());
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED:
                    state.speedX.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDX));
                    state.speedY.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDY));
                    state.speedZ.set((Double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_SPEEDCHANGED_SPEEDZ));
                    eventMap.put("speedX", state.speedX.get());
                    eventMap.put("speedY", state.speedY.get());
                    eventMap.put("speedZ", state.speedZ.get());
                    handler.post(() -> notifyEvent(eventMap));
                    if (!isLanded()) {
                        state.log("commandReceived " + commandKey.name());
                    }
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_ALTITUDECHANGED:
                    state.takeOffAltitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_ALTITUDECHANGED_ALTITUDE));
                    eventMap.put("takeOffAltitude", state.takeOffAltitude.get());
                    handler.post(() -> notifyEvent(eventMap));
                    if (!isLanded()) {
                        state.log("commandReceived " + commandKey.name());
                    }
                    break;
//                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED:
//                    state.gpsLatitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_LATITUDE));
//                    state.gpsLongitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_LONGITUDE));
//                    state.seaLevelAltitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_ALTITUDE));
//                    state.gpsLatitudeAccuracy.set((Integer)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_LATITUDE_ACCURACY));
//                    state.gpsLongitudeAccuracy.set((Integer)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_LONGITUDE_ACCURACY));
//                    state.seaLevelAltitudeAccuracy.set((Integer)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_GPSLOCATIONCHANGED_ALTITUDE_ACCURACY));
//                    eventMap.put("gpsLatitude", state.gpsLatitude.get());
//                    eventMap.put("gpsLongitude", state.gpsLongitude.get());
//                    eventMap.put("seaLevelAltitude", state.seaLevelAltitude.get());
//                    eventMap.put("gpsLatitudeAccuracy", state.gpsLatitudeAccuracy.get());
//                    eventMap.put("gpsLongitudeAccuracy", state.gpsLongitudeAccuracy.get());
//                    eventMap.put("seaLevelAltitudeAccuracy", state.seaLevelAltitudeAccuracy.get());
//                    handler.post(() -> notifyEvent(eventMap));
//                    if (!isLanded()) {
//                        state.log("commandReceived " + commandKey.name());
//                    }
//                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED:
                    state.gpsLatitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED_LATITUDE));
                    state.gpsLongitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED_LONGITUDE));
                    state.seaLevelAltitude.set((double)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED_ALTITUDE));
                    eventMap.put("gpsLatitude", state.gpsLatitude.get());
                    eventMap.put("gpsLongitude", state.gpsLongitude.get());
                    eventMap.put("seaLevelAltitude", state.seaLevelAltitude.get());
                    handler.post(() -> notifyEvent(eventMap));
                    if (!isLanded()) {
                        state.log("commandReceived " + commandKey.name());
                    }
                    break;
                case ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_GPSSTATE_NUMBEROFSATELLITECHANGED:
                    state.gpsSatelliteNumber.set((Integer)args.get(ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_GPSSTATE_NUMBEROFSATELLITECHANGED_NUMBEROFSATELLITE));
                    eventMap.put("gpsSatelliteNumber", state.gpsSatelliteNumber.get());
                    handler.post(() -> notifyEvent(eventMap));
                    state.log("commandReceived " + commandKey.name());
                    break;
            }

        }
    };

    private Map<String, Object> createEventMap(String event) {
        LinkedHashMap<String, Object> eventMap = new LinkedHashMap<>();
        long now  = new Date().getTime();
        eventMap.put("event", event);
        eventMap.put("ts", now);
        eventMap.put("runId", state.currentRunId.get());
        eventMap.put("flyingState", safeName(state.flyingState.get()));
        eventMap.put("droneState", safeName(state.droneState.get()));
        eventMap.put("sc2State", safeName(state.skyController2State.get()));
        return eventMap;
    }

    private String safeName(Enum e) {
        if (e == null) {
            return null;
        } else {
            return e.name();
        }
    }

    public boolean isLanded() {
        return state.flyingState.get() == ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED;
    }

    private final ARDeviceControllerStreamListener mStreamListener = new ARDeviceControllerStreamListener() {
        @Override
        public ARCONTROLLER_ERROR_ENUM configureDecoder(ARDeviceController deviceController,
                                                        final ARControllerCodec codec) {
            notifyConfigureDecoder(codec);
            return ARCONTROLLER_OK;
        }

        @Override
        public ARCONTROLLER_ERROR_ENUM onFrameReceived(ARDeviceController deviceController, final ARFrame frame) {
            notifyFrameReceived(frame);
            return ARCONTROLLER_OK;
        }

        @Override
        public void onFrameTimeout(ARDeviceController deviceController) {
        }
    };
}
