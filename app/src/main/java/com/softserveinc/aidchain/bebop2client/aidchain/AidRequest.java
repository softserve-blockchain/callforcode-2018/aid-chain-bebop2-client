package com.softserveinc.aidchain.bebop2client.aidchain;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

/**
 * @author ashapoch
 * @since 9/2/18.
 *
 * {
 *     "$class": "org.aid.processing.AidRequest",
 *     "description": "compass radio",
 *     "requestStatus": "APPROVED",
 *     "seeker": "resource:org.aid.processing.AidSeeker#jane-dean",
 *     "requiredBalance": 0,
 *     "currentBalance": 0,
 *     "targetLocation": {
 *       "$class": "org.aid.processing.Location",
 *       "latitude": 48.26394,
 *       "longitude": 25.94772
 *     },
 *     "creationTime": "2018-09-01T18:36:09.919Z",
 *     "entityId": "e86a7df0-d4f9-481c-a4be-cc94085bf701"
 *   }
 */
public class AidRequest extends ChainEntity {
    private static final String ENTITY_CLASS = "org.aid.processing.AidRequest";

    private final String requestStatus;
    private final String seeker;
    private final GeoLocation targetLocation;
    private final Date creationTime;

    public AidRequest(String entityId, String requestStatus, String seeker, GeoLocation targetLocation,
                      Date creationTime) {
        super(entityId);
        this.requestStatus = requestStatus;
        this.seeker = seeker;
        this.targetLocation = targetLocation;
        this.creationTime = creationTime;
    }

    @Override
    public String getEntityClass() {
        return ENTITY_CLASS;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public String getSeeker() {
        return seeker;
    }

    public GeoLocation getTargetLocation() {
        return targetLocation;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public LatLng getLocationAsLatLng() {
        if (targetLocation != null)
            return new LatLng(targetLocation.getLatitude(), targetLocation.getLongitude());
        else
            return null;
    }
}
