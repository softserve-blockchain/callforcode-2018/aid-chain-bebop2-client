package com.softserveinc.aidchain.bebop2client.aidchain;

/**
 * @author ashapoch
 * @since 9/2/18.
 *
 * {'$class': 'org.aid.processing.AidBucket',
 *  'aidRequest': 'resource:org.aid.processing.AidRequest#fe3b1e8c-62c1-40ac-88c2-77840f7471cc',
 *  'bucketStatus': 'INVENTORY',
 *  'commodities': ['resource:org.aid.processing.AidCommodity#water-purifier-tabs'],
 *  'creationTime': '2018-09-01T20:08:15.158Z',
 *  'entityId': '8c8a2dad-8b82-4252-a18f-054a5bec39f2',
 *  'inventory': 'resource:org.aid.processing.Inventory#chernivtsi-1'}
 */
public class AidBucket extends ChainEntity {
    private static final String ENTITY_CLASS = "org.aid.processing.AidBucket";

    private final String aidRequest;
    private final String bucketStatus;


    public AidBucket(String entityId, String aidRequest, String bucketStatus) {
        super(entityId);
        this.aidRequest = aidRequest;
        this.bucketStatus = bucketStatus;
    }

    @Override
    public String getEntityClass() {
        return ENTITY_CLASS;
    }

    public String getAidRequest() {
        return aidRequest;
    }

    public String getBucketStatus() {
        return bucketStatus;
    }
}
