package com.softserveinc.aidchain.bebop2client.aidchain;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * @author ashapoch
 * @since 8/31/18.
 */
public interface AidChainRest {
    @GET("Inventory")
    Call<List<Inventory>> readInventories();

    @GET("AidBucket")
    Call<List<AidBucket>> readAidBuckets(@Query("filter") String filter);

    @GET("AidRequest/{id}")
    Call<AidRequest> readAidRequest(@Path("id") String aidRequestId);

    @POST("StartBucketDeliveryTransaction")
    Call<StartBucketDeliveryTransaction> startBucketDeliveryTransaction(@Body StartBucketDeliveryTransaction tx);

    @POST("FinishBucketDeliveryTransaction")
    Call<FinishBucketDeliveryTransaction> finishBucketDeliveryTransaction(@Body FinishBucketDeliveryTransaction tx);
}
