package com.softserveinc.aidchain.bebop2client.aidchain;

/**
 * @author ashapoch
 * @since 8/31/18.
 */
public abstract class ChainEntity {
    private final String entityId;

    public ChainEntity(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public abstract String getEntityClass();

    public String getLongId() {
        return String.format("%s#%s", getEntityClass(), getEntityId());
    }
}
