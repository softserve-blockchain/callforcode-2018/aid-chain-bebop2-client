package com.softserveinc.aidchain.bebop2client.iot;

import android.content.Context;
import android.util.Log;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.*;

/**
 * @author ashapoch
 * @since 8/25/18.
 */
public class IoTClient {
    private static final String TAG = IoTClient.class.getSimpleName();

    private static final String IOT_ORGANIZATION_TCP = ".messaging.internetofthings.ibmcloud.com:1883";
    private static final String IOT_DEVICE_USERNAME  = "use-token-auth";

    private MqttAndroidClient mqttAndroidClient;
    private String organizationId = "68wz2w";
    private String deviceType = "dronepad";
    private String deviceId = "aidchainlenovo";
    private String authToken = "aidchaintoken";

    public IoTClient(Context context) {
        String clientId = "d:" + organizationId + ":" + deviceType + ":" + deviceId;
        String serverUri = "tcp://" + organizationId + IOT_ORGANIZATION_TCP;
        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
//                    addToHistory("Reconnected to : " + serverURI);
                    // Because Clean Session is true, we need to re-subscribe
//                    subscribeToTopic();
                } else {
//                    addToHistory("Connected to: " + serverURI);
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
//                addToHistory("The Connection was lost.");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                addToHistory("Incoming message: " + new String(message.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setUserName(IOT_DEVICE_USERNAME);
        mqttConnectOptions.setPassword(authToken.toCharArray());

        try {
            //addToHistory("Connecting to " + serverUri);
            mqttAndroidClient.connect(mqttConnectOptions, context, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
//                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    addToHistory("Failed to connect to: " + serverUri);
                    Log.e(TAG, "failed to connect to mqqt server", exception);
                }
            });

        } catch (MqttException ex){
            Log.e(TAG, "Exception when connecting to IBM IoT", ex);
        }
    }

    public void publishEvent(String event, String payload){
        if (!mqttAndroidClient.isConnected()) {
            Log.w(TAG, "Skip event publish since not connected to Mqqt Server");
            return;
        }
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(payload.getBytes());
            String publishTopic = getEventTopic(event);
            mqttAndroidClient.publish(publishTopic, message);
//            addToHistory("Message Published");
            if(!mqttAndroidClient.isConnected()){
//                addToHistory(mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.");
            }
        } catch (MqttException e) {
            Log.e(TAG, "Exception when publishing event " + event + " with payload " + payload, e);
        }
    }

    /**
     * @param event     The event to create a topic string for
     * @return The event topic for the specified event string
     */
    public static String getEventTopic(String event) {
        return "iot-2/evt/" + event + "/fmt/json";
    }

    public void dispose() {
        if (mqttAndroidClient != null) {
            mqttAndroidClient.unregisterResources();
            mqttAndroidClient.close();
        }
    }
}
