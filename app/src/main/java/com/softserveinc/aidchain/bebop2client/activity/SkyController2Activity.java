package com.softserveinc.aidchain.bebop2client.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;
import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM;
import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARControllerCodec;
import com.parrot.arsdk.arcontroller.ARFrame;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.softserveinc.aidchain.bebop2client.R;
import com.softserveinc.aidchain.bebop2client.aidchain.*;
import com.softserveinc.aidchain.bebop2client.drone.SkyController2Drone;
import com.softserveinc.aidchain.bebop2client.iot.IoTClient;
import com.softserveinc.aidchain.bebop2client.view.H264VideoView;
import org.apache.commons.collections4.QueueUtils;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SkyController2Activity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "SkyController2Activity";
    private SkyController2Drone skyController2Drone;
    private IoTClient ioTClient;
    private Gson mqttPayloadSerializer;

    private ProgressDialog connectionProgressDialog;
    private ProgressDialog downloadProgressDialog;

    private H264VideoView videoView;

    private TextView droneBatteryLabel;
    private TextView skyController2BatteryLabel;
    private Button takeOffLandBt;
    private TextView droneConnectionLabel;
    private Button downloadBt;
    private Button simulateButton;
    private Button deliveryButton;

    private int nbMaxDownload;
    private int currentDownloadIndex;
//    private TextView telemetryInfo;

    private GoogleMap googleMap;
    //    private CameraPosition cameraPosition;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient fusedLocationProviderClient;

    // A default location (Ukraine) and default zoom to use when location permission is
    // not granted.
    private final LatLng defaultLocation = new LatLng(48.3794, 31.1656);
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean locationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location lastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private Queue<Map<String, Object>> droneLocations = QueueUtils.synchronizedQueue(new CircularFifoQueue<>(100));

    private String currentRunId = null;
    private Inventory inventory;
    private IconGenerator requestIconFactory;
    private IconGenerator deliveringIconFactory;
    private DateFormat dateFormat;
    private Marker currentDeliveryMarker;
    private Marker pathMarker;
    private AidBucket loadedBucket;
    private boolean deliveryInProgress;
    private Map<String, DeliveryData> deliveries = new LinkedHashMap<>();

    private class DroneRun {
        private String runId;
        private List<LatLng> locations = new ArrayList<>();
//        private List<Marker> markers = new ArrayList<>();
        private int currentLocationIndex = 0;
        private double baseAltitude;

        public DroneRun() {
        }

        DroneRun(String runId, LatLng from, LatLng to, double baseAltitude) {
            this.runId = runId;
            locations = computePath(from, to);
            this.baseAltitude = baseAltitude;
        }

        boolean isComplete() {
            return locations.isEmpty() || currentLocationIndex >= locations.size();
        }

        Map<String, Object> nextEvent() {
            /*
              {
                "event": "ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED",
                "ts": 1535263722237,
                "runId": "AEA157",
                "flyingState": "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING",
                "droneState": "ARCONTROLLER_DEVICE_STATE_RUNNING",
                "sc2State": "ARCONTROLLER_DEVICE_STATE_RUNNING",
                "gpsLatitude": 48.25662163406961,
                "gpsLongitude": 25.947581395998643,
                "seaLevelAltitude": 245.3000030517578
              }
             */
            if (isComplete())
                return null;
            Map<String, Object> event = new LinkedHashMap<>();
            String flyingState = null;
            double seaLevelAltitude = 0;
            if (currentLocationIndex == 0) {
                flyingState = "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_TAKINGOFF";
                seaLevelAltitude = baseAltitude;
            } else if (currentLocationIndex == locations.size() - 1) {
                flyingState = "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDING";
                seaLevelAltitude = baseAltitude;
            } else {
                flyingState = "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING";
                seaLevelAltitude = baseAltitude + 10;
            }
            final LatLng loc = locations.get(currentLocationIndex);
            event.put("event", "ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED");
            event.put("ts", new Date().getTime());
            event.put("runId", runId);
            event.put("flyingState", flyingState);
            event.put("droneState", "ARCONTROLLER_DEVICE_STATE_RUNNING");
            event.put("sc2State", "ARCONTROLLER_DEVICE_STATE_RUNNING");
            event.put("gpsLatitude", loc.latitude);
            event.put("gpsLongitude", loc.longitude);
            event.put("seaLevelAltitude", seaLevelAltitude);
            currentLocationIndex++;
            return event;
        }
    }

    //runs without a timer by reposting this handler at the end of the runnable
    private Handler markPathTimerHandler = new Handler();
    private Runnable markPathTimerRunnable = new Runnable() {

        @Override
        public void run() {
            final Map<String, Object> positionEvent = droneLocations.poll();
            if (positionEvent != null) {
                markDeliveryPath(positionEvent);
            }
            markPathTimerHandler.postDelayed(this, 1000);
        }
    };

    private Handler updateDeliveryLocationsTimerHandler = new Handler();
    private Runnable updateDeliveryLocationsTimerRunnable = new Runnable() {

        @Override
        public void run() {
            updateDeliveryLocations();
            updateDeliveryLocationsTimerHandler.postDelayed(this, 60*1000);
        }
    };

    private Handler simulateRunTimerHandler = new Handler();
    private RunSimulation simulateRunTimerRunnable = new RunSimulation();

    private class RunSimulation implements Runnable {
        private DroneRun currentRun;

        void setCurrentRun(DroneRun run) {
            currentRun = run;
        }

        @Override
        public void run() {
            if (currentRun == null || currentRun.isComplete())
                return;
            final Map<String, Object> runEvent = currentRun.nextEvent();
            if (runEvent != null) {
                processDroneEvent(runEvent);
            }
            simulateRunTimerHandler.postDelayed(this, 1000);
        }
    }

    private Random random = new Random();

    private final String currentDroneId = "test-bebop2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skycontroller2);

        dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

        Intent intent = getIntent();
        ARDiscoveryDeviceService service = intent.getParcelableExtra(DeviceListActivity.EXTRA_DEVICE_SERVICE);
        inventory = intent.getParcelableExtra(InventoryListActivity.EXTRA_INVENTORY);

        initIHM();
        mqttPayloadSerializer = new GsonBuilder().create();

        ioTClient = new IoTClient(this);
        if (service.getProductID() != DeviceListActivity.DRONE_SIMULATOR){
            skyController2Drone = new SkyController2Drone(this, service);
            skyController2Drone.addListener(skyController2Listener);
        }

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            //            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        // Construct a FusedLocationProviderClient.
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);

        simulateButton = findViewById(R.id.simulateButton);
        simulateButton.setEnabled(false);
        simulateButton.setOnClickListener(this::startSimulation);

        deliveryButton = findViewById(R.id.deliveryButton);
        deliveryButton.setEnabled(false);
        deliveryButton.setOnClickListener(this::startDelivery);

    }

    private void startDelivery(View view) {
        if (currentDeliveryMarker == null || currentDeliveryMarker.getTag() == null)
            return;
        DeliveryData deliveryData = (DeliveryData) currentDeliveryMarker.getTag();
        if (deliveryData.getBucketCount() == 0)
            return;
        loadedBucket = deliveryData.aidBuckets.get(0);
        deliveryInProgress = true;
        final StartBucketDeliveryTransaction tx =
                new StartBucketDeliveryTransaction(loadedBucket.getEntityId(), currentDroneId, currentRunId);
        final Call<StartBucketDeliveryTransaction> startBucketDeliveryTransactionCall =
                AidChainClient.getInstance().getAidChainRest().startBucketDeliveryTransaction(tx);
        startBucketDeliveryTxInProgress = true;
        startBucketDeliveryTransactionCall.enqueue(new Callback<StartBucketDeliveryTransaction>() {
            @Override
            public void onResponse(@NonNull Call<StartBucketDeliveryTransaction> call,
                                   @NonNull Response<StartBucketDeliveryTransaction> response) {
                startBucketDeliveryTxCommitted.add(loadedBucket.getEntityId());
                startBucketDeliveryTxInProgress = false;
                deliveryButton.setOnClickListener(SkyController2Activity.this::cancelDelivery);
                deliveryButton.setText("Cancel Delivery");
                Log.i(TAG, "tx succeeded: " + tx);
            }

            @Override
            public void onFailure(@NonNull Call<StartBucketDeliveryTransaction> call, @NonNull Throwable t) {
                startBucketDeliveryTxInProgress = false;
                Log.e(TAG, "Error invoking tx: " + tx, t);
            }
        });
    }

    private void cancelDelivery(View view) {
        deliveryInProgress = false;
        deliveryButton.setOnClickListener(this::startDelivery);
    }

    private void finishDelivery(View view) {
        if (currentDeliveryMarker == null || currentDeliveryMarker.getTag() == null || loadedBucket == null)
            return;
        DeliveryData deliveryData = (DeliveryData) currentDeliveryMarker.getTag();
        if (deliveryData.getBucketCount() == 0)
            return;
        deliveryInProgress = false;
        deliveryButton.setOnClickListener(this::startDelivery);
        deliveryButton.setText("Start Delivery");
        deliveryButton.setEnabled(false);
        final AidBucket bucket = loadedBucket;
        loadedBucket = null;
        final FinishBucketDeliveryTransaction tx1 =
                new FinishBucketDeliveryTransaction(bucket.getEntityId(), currentDroneId, currentRunId);
        final Call<FinishBucketDeliveryTransaction> finishBucketDeliveryTransactionCall =
                AidChainClient.getInstance().getAidChainRest().finishBucketDeliveryTransaction(tx1);
        deliveryData.aidBuckets.remove(bucket);
        currentDeliveryMarker.setIcon(BitmapDescriptorFactory.fromBitmap(requestIconFactory.makeIcon(deliveryData.getMarkerText())));
        currentDeliveryMarker = null;
        finishBucketDeliveryTxInProgress = true;

        finishBucketDeliveryTransactionCall.enqueue(new Callback<FinishBucketDeliveryTransaction>() {
            @Override
            public void onResponse(@NonNull Call<FinishBucketDeliveryTransaction> call,
                                   @NonNull Response<FinishBucketDeliveryTransaction> response) {
                finishBucketDeliveryTxCommitted.add(bucket.getEntityId());
                finishBucketDeliveryTxInProgress = false;
                updateDeliveryLocations();
                Log.i(TAG, "tx succeeded: " + tx1);
            }
            @Override
            public void onFailure(@NonNull Call<FinishBucketDeliveryTransaction> call, @NonNull Throwable t) {
                finishBucketDeliveryTxInProgress = false;
                Log.e(TAG, "Error invoking tx: " + tx1, t);
            }
        });
    }

    private void startSimulation(View view) {
        Log.i(TAG, "last known location: " + lastKnownLocation);
        if (lastKnownLocation == null || currentDeliveryMarker == null)
            return;
        DeliveryData data = (DeliveryData) currentDeliveryMarker.getTag();
        if (data == null)
            return;
        LatLng from = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        LatLng to = data.getLocation();

//        double latDelta = (random.nextInt(19) - 9) * 0.001;
//        double lonDelta = (random.nextInt(19) - 9) * 0.001;
//        LatLng to = new LatLng(
//                from.latitude + latDelta,
//                from.longitude + lonDelta);
//        markDeliveryLocation(to, 1, new Date().toString());
        if (to == null)
            return;
        LatLng actualTo = new LatLng(
                to.latitude + random.nextGaussian()*0.00012,
                to.longitude + random.nextGaussian()*0.00012);
        String newRunId = "SIM-" + randomString(6);
        final DroneRun droneRun = new DroneRun(newRunId, from, actualTo, lastKnownLocation.getAltitude());
        simulateRunTimerRunnable.setCurrentRun(droneRun);
        simulateRunTimerHandler.postDelayed(simulateRunTimerRunnable, 0);
    }

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private String randomString(int len){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( random.nextInt(AB.length()) ) );
        return sb.toString();
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (googleMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, googleMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, lastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        requestIconFactory = new IconGenerator(this);
        requestIconFactory.setStyle(IconGenerator.STYLE_RED);
        deliveringIconFactory = new IconGenerator(this);
        deliveringIconFactory.setStyle(IconGenerator.STYLE_GREEN);
        googleMap.setOnMarkerClickListener(this::processMarkerClick);


        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        simulateButton.setEnabled(false);

        updateDeliveryLocations();
        markPathTimerHandler.postDelayed(markPathTimerRunnable, 1200);
        updateDeliveryLocationsTimerHandler.postDelayed(updateDeliveryLocationsTimerRunnable, 60*000);
    }

    private boolean processMarkerClick(@NonNull Marker marker) {
        if (deliveryInProgress)
            return false;
        else
            deliveryButton.setEnabled(true);
        if (marker == currentDeliveryMarker)
            return false;
        if (marker.getTag() == null || ((DeliveryData)marker.getTag()).getBucketCount() == 0) {
            return false;
        }
        if (currentDeliveryMarker != null && currentDeliveryMarker.getTag() != null) {
            String markerText = ((DeliveryData)currentDeliveryMarker.getTag()).getMarkerText();
            currentDeliveryMarker.setIcon(BitmapDescriptorFactory.fromBitmap(requestIconFactory.makeIcon(markerText)));
        }
        currentDeliveryMarker = marker;
        if (currentDeliveryMarker.getTag() != null) {
            String markerText = ((DeliveryData) currentDeliveryMarker.getTag()).getMarkerText();
            currentDeliveryMarker.setIcon(BitmapDescriptorFactory.fromBitmap(deliveringIconFactory.makeIcon(markerText)));
            simulateButton.setEnabled(true);
        }
        return false;
    }

    private class DeliveryData {
        private Marker deliveryMarker;
        private Circle dropCircle;
        private AidRequest aidRequest;
        private List<AidBucket> aidBuckets = new ArrayList<>();

        private int getBucketCount() {
            return aidBuckets.size();
        }

        private String formatCreationTime() {
            try {
                return dateFormat.format(aidRequest.getCreationTime());
            } catch (Exception e) {
                Log.e(TAG, "Error formatting request creation time for request: " + aidRequest.getEntityId(), e);
                return "Time unknown";
            }
        }

        private LatLng getLocation() {
            return aidRequest.getLocationAsLatLng();
        }

        private String getMarkerText() {
            return formatCreationTime() + " || " + getBucketCount() + "b";
        }
    }

    private void updateDeliveryLocations() {
        if (inventory != null) {
            final Call<List<AidBucket>> readInventoryAidBuckets =
                    AidChainClient.getInstance().readInventoryAidBuckets(inventory.getEntityId());
            readInventoryAidBuckets.enqueue(new Callback<List<AidBucket>>() {
                @Override
                public void onResponse(@NonNull Call<List<AidBucket>> call, @NonNull Response<List<AidBucket>> response) {
                    List<AidBucket> buckets = response.body();
                    if (buckets != null) {
                        final Set<String> aidRequestIds =
                                buckets.stream().map(AidBucket::getAidRequest)
                                        .map(ref -> ref.split("#")[1]).collect(Collectors.toSet());
                        Set<String> requestsToRemove = new HashSet<>(deliveries.keySet());
                        requestsToRemove.removeAll(aidRequestIds);
                        requestsToRemove.forEach(rid -> {
                            DeliveryData deliveryData = deliveries.remove(rid);
                            if (deliveryData != null) {
                                deliveryData.dropCircle.remove();
                                deliveryData.deliveryMarker.remove();
                                if (currentDeliveryMarker == deliveryData.deliveryMarker) {
                                    currentDeliveryMarker = null;
                                }
                            }
                        });
                        final List<Call<AidRequest>> aidRequestCalls =
                                aidRequestIds.stream()
                                        .map(id -> AidChainClient.getInstance().getAidChainRest().readAidRequest(id))
                                        .collect(Collectors.toList());
                        aidRequestCalls.forEach(aidRequestCall -> {
                            aidRequestCall.enqueue(new Callback<AidRequest>() {
                                @Override
                                public void onResponse(@NonNull Call<AidRequest> call, @NonNull Response<AidRequest> response) {
                                    AidRequest aidRequest = response.body();
                                    if (aidRequest != null) {
                                        LatLng deliveryLocation = aidRequest.getLocationAsLatLng();
                                        if (deliveryLocation != null) {
                                            DeliveryData deliveryData = deliveries.get(aidRequest.getEntityId());
                                            if (deliveryData == null) {
                                                deliveryData = new DeliveryData();
                                                deliveries.put(aidRequest.getEntityId(), deliveryData);
                                            }
                                            deliveryData.aidRequest = aidRequest;
                                            deliveryData.aidBuckets = buckets.stream()
                                                    .filter(b -> b.getAidRequest().endsWith("#" + aidRequest.getEntityId()))
                                                    .collect(Collectors.toList());
                                            markDeliveryLocation(deliveryData);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<AidRequest> call, @NonNull Throwable t) {
                                    Log.e(TAG, "Error reading aid request for: " + call.request().url().encodedPath(), t);
                                }
                            });
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<AidBucket>> call, @NonNull Throwable t) {
                    Log.e(TAG, "Error reading aid buckets from aid chain for inventory: " + inventory.getEntityClass(), t);
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // show a loading view while the bebop drone is connecting
        if ((skyController2Drone != null) &&
                !(ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals(skyController2Drone.getSkyController2ConnectionState()))) {
            connectionProgressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            connectionProgressDialog.setIndeterminate(true);
            connectionProgressDialog.setMessage("Connecting ...");
            connectionProgressDialog.setCancelable(false);
            connectionProgressDialog.show();

            // if the connection to the Bebop fails, finish the activity
            if (!skyController2Drone.connect()) {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (skyController2Drone != null) {
            connectionProgressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            connectionProgressDialog.setIndeterminate(true);
            connectionProgressDialog.setMessage("Disconnecting ...");
            connectionProgressDialog.setCancelable(false);
            connectionProgressDialog.show();

            if (!skyController2Drone.disconnect()) {
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        markPathTimerHandler.removeCallbacks(markPathTimerRunnable);
        simulateRunTimerHandler.removeCallbacks(simulateRunTimerRunnable);
    }

    @Override
    public void onDestroy() {
        if (skyController2Drone != null) {
            skyController2Drone.dispose();
        }
        if (ioTClient != null) {
            ioTClient.dispose();
        }
        super.onDestroy();
    }

    private void initIHM() {
        videoView = findViewById(R.id.videoView);

        videoView.setVisibility(View.INVISIBLE);

        findViewById(R.id.emergencyBt).setOnClickListener(v -> {
            if (skyController2Drone != null)
                skyController2Drone.emergency();
        });

        takeOffLandBt = findViewById(R.id.takeOffOrLandBt);
        takeOffLandBt.setOnClickListener(v -> {
            if (skyController2Drone != null) {
                switch (skyController2Drone.getFlyingState()) {
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED:
                        skyController2Drone.takeOff();
                        break;
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING:
                    case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING:
                        skyController2Drone.land();
                        break;
                    default:
                }
            }
        });

        findViewById(R.id.takePictureBt).setOnClickListener(v -> {
            if (skyController2Drone != null) {
                skyController2Drone.takePicture();
            }
        });

        downloadBt = findViewById(R.id.downloadBt);
        downloadBt.setEnabled(false);
        downloadBt.setOnClickListener(v -> {
            if (skyController2Drone != null) {
                skyController2Drone.getLastFlightMedias();

                downloadProgressDialog = new ProgressDialog(SkyController2Activity.this,
                        R.style.AppCompatAlertDialogStyle);
                downloadProgressDialog.setIndeterminate(true);
                downloadProgressDialog.setMessage("Fetching medias");
                downloadProgressDialog.setCancelable(false);
                downloadProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                        (dialog, which) -> skyController2Drone.cancelGetLastFlightMedias());
                downloadProgressDialog.show();
            }
        });

        skyController2BatteryLabel = findViewById(R.id.skyBatteryLabel);
        droneBatteryLabel = findViewById(R.id.droneBatteryLabel);

        droneConnectionLabel = findViewById(R.id.droneConnectionLabel);
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, task -> {

                    LatLng newLocation = null;
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.getResult();
                        if (lastKnownLocation != null) {
                            newLocation = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
                        }
                    }
                    if (newLocation == null) {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        newLocation = defaultLocation;
                        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            newLocation, DEFAULT_ZOOM));

                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (googleMap == null) {
            return;
        }
        try {
            if (locationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                lastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void markDeliveryLocation(DeliveryData deliveryData) {
        LatLng deliveryLocation = deliveryData.getLocation();
        if (deliveryData.dropCircle != null) {
            if (!deliveryData.dropCircle.getCenter().equals(deliveryLocation)) {
                deliveryData.dropCircle.setCenter(deliveryLocation);
            }
        } else {
            // Instantiates a new CircleOptions object and defines the center and radius
            CircleOptions circleOptions = new CircleOptions()
                    .center(deliveryLocation)
                    .fillColor(0x33ff0000)
                    .strokeColor(Color.RED)
                    .strokeWidth(2)
                    .zIndex(0.8f)
                    .visible(true)
                    .radius(50); // In meters

            // Get back the mutable Circle
            Circle circle = googleMap.addCircle(circleOptions);
            circle.setVisible(true);
            deliveryData.dropCircle = circle;
        }

        final String markerText = deliveryData.getMarkerText();
        if (deliveryData.deliveryMarker != null) {
            if (!deliveryData.deliveryMarker.getPosition().equals(deliveryLocation))
                deliveryData.deliveryMarker.setPosition(deliveryLocation);
            DeliveryData currentDeliveryData = currentDeliveryMarker != null ? (DeliveryData) currentDeliveryMarker.getTag() : null;
            if (currentDeliveryData != null && currentDeliveryData.aidRequest.getEntityId().contentEquals(deliveryData.aidRequest.getEntityId()))
                deliveryData.deliveryMarker.setIcon(BitmapDescriptorFactory.fromBitmap(deliveringIconFactory.makeIcon(markerText)));
            else
                deliveryData.deliveryMarker.setIcon(BitmapDescriptorFactory.fromBitmap(requestIconFactory.makeIcon(markerText)));
        } else {
            Marker marker = addIcon(markerText, deliveryLocation);
            marker.setTag(deliveryData);
            marker.setZIndex(0.9f);
            marker.setAlpha(0.5f);
            deliveryData.deliveryMarker = marker;
        }

//        Marker deliveryMarker = googleMap.addMarker(new MarkerOptions()
//                .position(deliveryLocation));
    }

    private void markDeliveryPath(Map<String, Object> positionEvent) {
        final Double gpsLatitude = (Double) positionEvent.get("gpsLatitude");
        final Double gpsLongitude = (Double) positionEvent.get("gpsLongitude");
        if (gpsLatitude != null && gpsLongitude != null) {
            LatLng loc = new LatLng(
                    gpsLatitude,
                    gpsLongitude);

            if (pathMarker != null) {
                pathMarker.setPosition(loc);
            } else {
                final MarkerOptions markerOps = new MarkerOptions()
                        .position(loc)
                        .zIndex(0.99f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.cross));
                pathMarker = googleMap.addMarker(markerOps);
            }
        }
    }

    private List<LatLng> computePath(LatLng from, LatLng to) {
        double distance = Math.sqrt(
                Math.pow(from.latitude - to.latitude, 2) + Math.pow(from.longitude - to.longitude, 2));
        float[] distanceMeters = new float[1];
        Location.distanceBetween(from.latitude, from.longitude, to.latitude, to.longitude, distanceMeters);
        float speedMps = 10.0f;
        int size = Math.round(distanceMeters[0] / speedMps);
        List<LatLng> locations = new ArrayList<>(size+5);
        double increment = distance / size;
        for (int i = 1; i <= size; i++) {
            double t = increment * i;
            double lat = t * (to.latitude - from.latitude) / distance + from.latitude;
            double lon = t * (to.longitude - from.longitude) / distance + from.longitude;
            locations.add(new LatLng(lat, lon));
        }
        return locations;
    }


    private final SkyController2Drone.Listener skyController2Listener = new SkyController2Drone.Listener() {
        @Override
        public void onSkyController2ConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state) {
            switch (state) {
                case ARCONTROLLER_DEVICE_STATE_RUNNING:
                    connectionProgressDialog.dismiss();
                    // if no drone is connected, display a message
                    if (!ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals(skyController2Drone.getDroneConnectionState())) {
                        droneConnectionLabel.setVisibility(View.VISIBLE);
                        videoView.setVisibility(View.INVISIBLE);
                    }
                    break;

                case ARCONTROLLER_DEVICE_STATE_STOPPED:
                    // if the deviceController is stopped, go back to the previous activity
                    connectionProgressDialog.dismiss();
                    finish();
                    break;

                default:
                    break;
            }
        }

        @Override
        public void onDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state) {
            switch (state) {
                case ARCONTROLLER_DEVICE_STATE_RUNNING:
                    droneConnectionLabel.setVisibility(View.GONE);
                    videoView.setVisibility(View.VISIBLE);
                    break;

                default:
                    droneConnectionLabel.setVisibility(View.VISIBLE);
                    videoView.setVisibility(View.INVISIBLE);
                    break;
            }
        }

        @Override
        public void onSkyController2BatteryChargeChanged(int batteryPercentage) {
            skyController2BatteryLabel.setText(String.format(Locale.US,"%d%%", batteryPercentage));
        }

        @Override
        public void onDroneBatteryChargeChanged(int batteryPercentage) {
            droneBatteryLabel.setText(String.format(Locale.US, "%d%%", batteryPercentage));
        }

        @Override
        public void onPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state) {
            switch (state) {
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED:
                    takeOffLandBt.setText("Take off");
                    takeOffLandBt.setEnabled(true);
                    downloadBt.setEnabled(true);
                    break;
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING:
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING:
                    takeOffLandBt.setText("Land");
                    takeOffLandBt.setEnabled(true);
                    downloadBt.setEnabled(false);
                    break;
                default:
                    takeOffLandBt.setEnabled(false);
                    downloadBt.setEnabled(false);
            }
        }

        @Override
        public void onPictureTaken(ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error) {
            Log.i(TAG, "Picture has been taken");
        }

        @Override
        public void configureDecoder(ARControllerCodec codec) {
            videoView.configureDecoder(codec);
        }

        @Override
        public void onFrameReceived(ARFrame frame) {
            videoView.displayFrame(frame);
        }

        @Override
        public void onMatchingMediasFound(int nbMedias) {
            downloadProgressDialog.dismiss();

            nbMaxDownload = nbMedias;
            currentDownloadIndex = 1;

            if (nbMedias > 0) {
                downloadProgressDialog = new ProgressDialog(SkyController2Activity.this,
                        R.style.AppCompatAlertDialogStyle);
                downloadProgressDialog.setIndeterminate(false);
                downloadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                downloadProgressDialog.setMessage("Downloading medias");
                downloadProgressDialog.setMax(nbMaxDownload * 100);
                downloadProgressDialog.setSecondaryProgress(currentDownloadIndex * 100);
                downloadProgressDialog.setProgress(0);
                downloadProgressDialog.setCancelable(false);
                downloadProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                        (dialog, which) -> skyController2Drone.cancelGetLastFlightMedias());
                downloadProgressDialog.show();
            }
        }

        @Override
        public void onDownloadProgressed(String mediaName, int progress) {
            downloadProgressDialog.setProgress(((currentDownloadIndex - 1) * 100) + progress);
        }

        @Override
        public void onDownloadComplete(String mediaName) {
            currentDownloadIndex++;
            downloadProgressDialog.setSecondaryProgress(currentDownloadIndex * 100);

            if (currentDownloadIndex > nbMaxDownload) {
                downloadProgressDialog.dismiss();
                downloadProgressDialog = null;
            }
        }

        @Override
        public void onSpeedChanged(double speedX, double speedY, double speedZ) {
        }

        @Override
        public void onEvent(Map<String, Object> eventMap) {
            processDroneEvent(eventMap);
        }
    };

    private boolean startBucketDeliveryTxInProgress = false;
    private boolean finishBucketDeliveryTxInProgress = false;
    private final Set<String> startBucketDeliveryTxCommitted = new LinkedHashSet<>();
    private final Set<String> finishBucketDeliveryTxCommitted = new LinkedHashSet<>();

    private void processDroneEvent(Map<String, Object> eventMap) {
        if (eventMap.get("runId") == null) {
            return;
        }
        currentRunId = (String) eventMap.get("runId");
        String event = (String)eventMap.get("event");
        // publish event to mqtt
        if (ioTClient != null) {
            try {
                String eventName = "drone";
                final String payload = mqttPayloadSerializer.toJson(eventMap);
                Log.i(TAG, "publishing event: " + payload);
                ioTClient.publishEvent(eventName, payload);
            } catch (Exception e) {
                Log.e(TAG, "error publishing event to mqtt", e);
            }
        }
        // schedule for marking on the map
        if ("ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_POSITIONCHANGED".equals(event)) {
            droneLocations.add(eventMap);
            final Marker marker = currentDeliveryMarker;
            if (marker == null || marker.getTag() == null)
                return;
            DeliveryData deliveryData = (DeliveryData) marker.getTag();
            if (deliveryData.getBucketCount() == 0)
                return;
            final AidBucket aidBucket = deliveryData.aidBuckets.get(0);
            String flyingState = (String) eventMap.get("flyingState");
            if (flyingState != null) {
                switch (flyingState) {
                    case "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_TAKINGOFF":
                        break;
                    case "ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDING":
                        if (!finishBucketDeliveryTxInProgress && !finishBucketDeliveryTxCommitted.contains(aidBucket.getEntityId())) {
                            double lat = (double) eventMap.get("gpsLatitude");
                            double lng = (double) eventMap.get("gpsLongitude");
                            double distance = SphericalUtil.computeDistanceBetween(deliveryData.getLocation(), new LatLng(lat, lng));
                            if (distance < 50 && deliveryInProgress && !"Finish Delivery".contentEquals(deliveryButton.getText())) {
                                deliveryButton.setOnClickListener(this::finishDelivery);
                                deliveryButton.setText("Finish Delivery");
                            }
                        }
                        break;
                }
            }
        }
    }

    private Marker addIcon(CharSequence text, LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions().
                icon(BitmapDescriptorFactory.fromBitmap(requestIconFactory.makeIcon(text))).
                position(position).
                anchor(requestIconFactory.getAnchorU(), requestIconFactory.getAnchorV());

        return googleMap.addMarker(markerOptions);
    }
}
