package com.softserveinc.aidchain.bebop2client.aidchain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ashapoch
 * @since 8/31/18.
 */
public class AidChainClient {
    private static AidChainClient ourInstance = new AidChainClient();

    private final Retrofit retrofit;
    private final Gson gson;
    private final AidChainRest aidChainRest;

    public static AidChainClient getInstance() {
        return ourInstance;
    }

    private AidChainClient() {
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();
        retrofit = new Retrofit.Builder()
                .baseUrl("https://aid-chain-rest.eu-gb.mybluemix.net/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        aidChainRest = retrofit.create(AidChainRest.class);
    }

    public AidChainRest getAidChainRest() {
        return aidChainRest;
    }

    public Call<List<AidBucket>> readInventoryAidBuckets(String inventoryId) {
        // inv_bucket_filter = {"where": {"and":[{"inventory": "resource:org.aid.processing.Inventory#" + str(inventory_id)},{"bucketStatus": "INVENTORY"}]}}
        JsonObject inv = new JsonObject();
        inv.addProperty("inventory", "resource:org.aid.processing.Inventory#" + inventoryId);
        JsonObject st = new JsonObject();
        JsonArray or = new JsonArray();
        JsonObject st1 = new JsonObject();
        JsonObject st2 = new JsonObject();
        st1.addProperty("bucketStatus", "INVENTORY");
        st2.addProperty("bucketStatus", "INPROGRESS");
        or.add(st1);
        or.add(st2);
        st.add("or", or);
        JsonArray and = new JsonArray();
        and.add(inv);
        and.add(st);
        JsonObject andCond = new JsonObject();
        andCond.add("and", and);
        JsonObject where = new JsonObject();
        where.add("where", andCond);
        String filter = gson.toJson(where);
        return getAidChainRest().readAidBuckets(filter);
    }
}
