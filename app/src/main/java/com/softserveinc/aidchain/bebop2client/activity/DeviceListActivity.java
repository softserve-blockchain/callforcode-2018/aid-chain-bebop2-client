package com.softserveinc.aidchain.bebop2client.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.softserveinc.aidchain.bebop2client.R;
import com.softserveinc.aidchain.bebop2client.aidchain.Inventory;
import com.softserveinc.aidchain.bebop2client.drone.DroneDiscoverer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_SKYCONTROLLER_2;

public class DeviceListActivity extends AppCompatActivity {
    static final String EXTRA_DEVICE_SERVICE = "EXTRA_DEVICE_SERVICE";

    private static final String TAG = "DeviceListActivity";

    /**
     * List of runtime permission we need.
     */
    private static final String[] PERMISSIONS_NEEDED = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    /**
     * Code for permission request result handling.
     */
    private static final int REQUEST_CODE_PERMISSIONS_REQUEST = 1;

    private DroneDiscoverer droneDiscoverer;

    private final List<ARDiscoveryDeviceService> droneList = new ArrayList<>();

    // this block loads the native libraries
    // it is mandatory
    static {
        ARSDK.loadSDKLibs();
    }

    static final int DRONE_SIMULATOR = -255;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        final ListView listView = findViewById(R.id.list);

        // Assign adapter to ListView
        listView.setAdapter(itemAdapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            // launch the activity related to the type of discovery device service
            Intent intent = null;

            ARDiscoveryDeviceService service = (ARDiscoveryDeviceService) itemAdapter.getItem(position);
            ARDISCOVERY_PRODUCT_ENUM product;
            if (service.getProductID() == DRONE_SIMULATOR) {
                product = ARDISCOVERY_PRODUCT_SKYCONTROLLER_2;
            } else {
                product = ARDiscoveryService.getProductFromProductID(service.getProductID());
            }
            switch (product) {
                case ARDISCOVERY_PRODUCT_ARDRONE:
                case ARDISCOVERY_PRODUCT_BEBOP_2:
                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_2:
                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_2P:
                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_NG:
                    intent = new Intent(DeviceListActivity.this, InventoryListActivity.class);
                    break;

                default:
                    Log.e(TAG, "The type " + product + " is not supported by this sample");
            }

            if (intent != null) {
                intent.putExtra(EXTRA_DEVICE_SERVICE, service);
                startActivity(intent);
            }
        });

        droneDiscoverer = new DroneDiscoverer(this);

        Set<String> permissionsToRequest = new HashSet<>();
        for (String permission : PERMISSIONS_NEEDED) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    Toast.makeText(this, "Please allow permission " + permission, Toast.LENGTH_LONG).show();
                    finish();
                    return;
                } else {
                    permissionsToRequest.add(permission);
                }
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_CODE_PERMISSIONS_REQUEST);
        }

        droneList.add(createSimulator());
    }

    private ARDiscoveryDeviceService createSimulator() {
        final ARDiscoveryDeviceService service = new ARDiscoveryDeviceService();
        service.setName("Drone Delivery Simulation");
        service.setProductID(DRONE_SIMULATOR);
        return service;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // setup the drone discoverer and register as listener
        droneDiscoverer.setup();
        droneDiscoverer.addListener(mDiscovererListener);

        // start discovering
        droneDiscoverer.startDiscovering();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // clean the drone discoverer object
        droneDiscoverer.stopDiscovering();
        droneDiscoverer.cleanup();
        droneDiscoverer.removeListener(mDiscovererListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean denied = false;
        if (permissions.length == 0) {
            // canceled, finish
            denied = true;
        } else {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    denied = true;
                }
            }
        }

        if (denied) {
            Toast.makeText(this, "At least one permission is missing.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private final DroneDiscoverer.Listener mDiscovererListener = new DroneDiscoverer.Listener() {

        @Override
        public void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList) {
            droneList.clear();
            droneList.add(createSimulator());
            droneList.addAll(dronesList);

            itemAdapter.notifyDataSetChanged();
        }
    };



    private final BaseAdapter itemAdapter = new BaseAdapter() {
        int bebop2DrawableId = R.drawable.parrot_bebop_power;
        int sc2DrawableId = R.drawable.parrot_skycontroller_2;
        @Override
        public int getCount() {
            return droneList.size();
        }

        @Override
        public Object getItem(int position) {
            return droneList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // reuse views
            if (convertView == null) {
                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.list_row, parent, false);
            }

            TextView title;
            ImageView i1;
            i1 = convertView.findViewById(R.id.imgIcon);
            title = convertView.findViewById(R.id.txtTitle);
            ARDiscoveryDeviceService service = droneList.get(position);
            title.setText(service.getName() + " on " + service.getNetworkType());
            if (service.getName().equalsIgnoreCase("skycontroller 2"))
                i1.setImageResource(sc2DrawableId);
            else
                i1.setImageResource(bebop2DrawableId);

            return convertView;
        }
    };

}
