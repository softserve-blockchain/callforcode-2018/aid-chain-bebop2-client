package com.softserveinc.aidchain.bebop2client.aidchain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.StringJoiner;

/**
 * @author ashapoch
 * @since 8/31/18.
 */
public class Inventory extends ChainEntity implements Parcelable {
    private static final String ENTITY_CLASS = "org.aid.processing.Inventory";

    public static final Parcelable.Creator<Inventory> CREATOR = new Parcelable.Creator<Inventory>()
    {
        @Override
        public Inventory createFromParcel(Parcel source)
        {
            return new Inventory(source);
        }

        @Override
        public Inventory[] newArray(int size)
        {
            return new Inventory[size];
        }
    };

    private final String locationAddress;

    public Inventory(String entityId, String locationAddress) {
        super(entityId);
        this.locationAddress = locationAddress;
    }

    public Inventory(Parcel source) {
        this(source.readString(), source.readString());
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    @Override
    public String getEntityClass() {
        return ENTITY_CLASS;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Inventory.class.getSimpleName() + "[", "]")
                .add("longId='" + getLongId() + "'")
                .add("locationAddress='" + locationAddress + "'")
                .toString();
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled
     * by this Parcelable object instance.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getEntityId());
        dest.writeString(getLocationAddress());
    }
}
