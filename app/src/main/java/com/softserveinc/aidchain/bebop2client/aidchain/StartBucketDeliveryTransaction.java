package com.softserveinc.aidchain.bebop2client.aidchain;

import java.util.StringJoiner;

/**
 * @author ashapoch
 * @since 9/2/18.
 *
 * {
 *   "$class": "org.aid.processing.StartBucketDeliveryTransaction",
 *   "asset": "org.aid.processing.AidBucket#" + str(aid_bucket_id),
 *   "targetDrone": "org.aid.processing.Drone#" + str(drone_id)
 *  }
 */
public class StartBucketDeliveryTransaction {
    private final String $class = "org.aid.processing.StartBucketDeliveryTransaction";
    private final String asset;
    private final String targetDrone;
    private final String runId;

    public StartBucketDeliveryTransaction(String aidBucketId, String droneId, String runId) {
        this.asset = "org.aid.processing.AidBucket#" + aidBucketId;
        this.targetDrone = "org.aid.processing.Drone#" + droneId;
        this.runId = runId;
    }

    public String get$class() {
        return $class;
    }

    public String getAsset() {
        return asset;
    }

    public String getTargetDrone() {
        return targetDrone;
    }

    public String getRunId() {
        return runId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", StartBucketDeliveryTransaction.class.getSimpleName() + "[", "]")
                .add("$class='" + $class + "'")
                .add("asset='" + asset + "'")
                .add("targetDrone='" + targetDrone + "'")
                .add("runId='" + runId + "'")
                .toString();
    }
}
