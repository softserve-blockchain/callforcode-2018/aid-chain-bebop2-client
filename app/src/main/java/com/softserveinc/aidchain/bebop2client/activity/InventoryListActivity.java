package com.softserveinc.aidchain.bebop2client.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.softserveinc.aidchain.bebop2client.R;
import com.softserveinc.aidchain.bebop2client.aidchain.AidChainClient;
import com.softserveinc.aidchain.bebop2client.aidchain.Inventory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

import static com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM.ARDISCOVERY_PRODUCT_SKYCONTROLLER_2;

public class InventoryListActivity extends AppCompatActivity {
    private static final String TAG = InventoryListActivity.class.getSimpleName();

    static final String EXTRA_INVENTORY = "EXTRA_INVENTORY";
    private final List<Inventory> inventories = new ArrayList<>();
    private Button refreshButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_list);
        final ListView listView = findViewById(R.id.list);
        addOffChainInventory();

        // Assign adapter to ListView
        listView.setAdapter(itemAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            final Parcelable item = (Parcelable) itemAdapter.getItem(position);
            Intent intent = null;

            Intent prevIntent = getIntent();
            ARDiscoveryDeviceService service =
                    prevIntent.getParcelableExtra(DeviceListActivity.EXTRA_DEVICE_SERVICE);
            ARDISCOVERY_PRODUCT_ENUM product;
            if (service.getProductID() == DeviceListActivity.DRONE_SIMULATOR) {
                product = ARDISCOVERY_PRODUCT_SKYCONTROLLER_2;
            } else {
                product = ARDiscoveryService.getProductFromProductID(service.getProductID());
            }
            switch (product) {
                case ARDISCOVERY_PRODUCT_ARDRONE:
                case ARDISCOVERY_PRODUCT_BEBOP_2:
                    intent = new Intent(InventoryListActivity.this, BebopActivity.class);
                    break;


                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_2:
                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_2P:
                case ARDISCOVERY_PRODUCT_SKYCONTROLLER_NG:
                    intent = new Intent(InventoryListActivity.this, SkyController2Activity.class);
                    break;

                default:
                    Log.e(TAG, "The type " + product + " is not supported by this sample");
            }

            if (intent != null) {
                intent.putExtra(EXTRA_INVENTORY, item);
                intent.putExtra(DeviceListActivity.EXTRA_DEVICE_SERVICE, service);
                startActivity(intent);
            }
        });

        refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(v -> {
            refreshInventories();
        });
    }

    private void addOffChainInventory() {
        inventories.add(new Inventory("off-chain-inv", "Off-chain Inventory"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshInventories();
    }

    private void refreshInventories() {
        refreshButton.setEnabled(false);
        final Call<List<Inventory>> readInventories =
                AidChainClient.getInstance().getAidChainRest().readInventories();

        readInventories.enqueue(new Callback<List<Inventory>>() {
            @Override
            public void onResponse(@NonNull Call<List<Inventory>> call, @NonNull Response<List<Inventory>> response) {
                final List<Inventory> chainInventories = response.body();
                updateInventoryList(chainInventories);
                refreshButton.setEnabled(true);
            }

            @Override
            public void onFailure(@NonNull Call<List<Inventory>> call, @NonNull Throwable t) {
                updateInventoryList(null);
                refreshButton.setEnabled(true);
                Log.e(TAG, "Error reading inventories from aid chain", t);
            }
        });
    }

    private void updateInventoryList(List<Inventory> chainInventories) {
        inventories.clear();
        addOffChainInventory();
        if (chainInventories != null)
            inventories.addAll(chainInventories);
        itemAdapter.notifyDataSetChanged();
    }

    private final BaseAdapter itemAdapter = new BaseAdapter() {
        int inventoryDrawableId = R.drawable.pallet;
        @Override
        public int getCount() {
            return inventories.size();
        }

        @Override
        public Object getItem(int position) {
            return inventories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // reuse views
            if (convertView == null) {
                LayoutInflater inflater = getLayoutInflater();
                convertView = inflater.inflate(R.layout.list_row, parent, false);
            }

            TextView title;
            ImageView i1;
            i1 = convertView.findViewById(R.id.imgIcon);
            title = convertView.findViewById(R.id.txtTitle);
            Inventory inventory = (Inventory) getItem(position);
            title.setText(String.format("Inventory %s at %s", inventory.getEntityId(), inventory.getLocationAddress()));
            i1.setImageResource(inventoryDrawableId);
            return convertView;
        }
    };
}
